# MONGO TESTS


### Mongo Structure
---
#### Database: "SlippiTests"
##### ══ Collection: Players
Contains All Player Files
##### ══ Collection: Replays
Contains All Replay Files


### Example

```
SlippiTests
╚═> Players
  ╚═══> DOT#995
        ╚═══> Tag: "DotCD"
        ╚═══> Games: [0:"id", 1:"id2"]
  ╚═══> JEFF#241
        ╚═══> Tag: "Jeff"
╚═> Replays
  ╚═══> ReplayID1
        ╚═══> Kills
              ╚═══> Port1: 3
        ╚═══> Winner: Port0
  ╚═══> ReplayID2
        ╚═══> Stage: Pokemon Stadium
```