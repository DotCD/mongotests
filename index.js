const { MongoClient } = require('mongodb');
const uri = "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false";
const slippiDB = "SlippiTest";
const playerCollect = "Players";
const gamesCollect = "Replays";

const client = new MongoClient(uri);

async function getPlayerTag(connect) {
    let tag;
    try {
        //connects the client to mongoDB server
        await client.connect();

        //delcare that databse and player collection
        const db = client.db(slippiDB);
        const collection = db.collection(playerCollect);

        //sets the query
        const query = { ConnectCode: connect };

        //options for the find
        const options = {
            //sorts by connet code in descending order 
            //doesn't actually make sense here since the query is FindOne so it only returns one object but  good to know how to sort so I leave it in
            sort: { ConnectCode: -1},
            //make it so the document ID and games array are not returned
            projection: { _id: 0, Games: 0 },
        };

        //query the DB
        tag = await collection.findOne(query, options);

    } finally {
        //close the client connection
        await client.close();
        // return the tag
        return tag.Tag;
    }

}

function generatePlayer(Code, Name, Games) {

}


async function test() {
    const tag = await getPlayerTag("DOT#995");
    console.log(tag);
}

test();